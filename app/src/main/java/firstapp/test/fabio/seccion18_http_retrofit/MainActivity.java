package firstapp.test.fabio.seccion18_http_retrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*String json = "{ cod: 1," +
                "name : 'Fabio'  }";

        try {
            JSONObject mjosn = new JSONObject(json);
            int id = mjosn.getInt("cod");
            String name = mjosn.getString("name");

            City city = new City(id,name);

            System.out.println("citi: " + city.getId() + " " + city.getName());

        }catch (JSONException e){
            e.printStackTrace();
        }

        Gson gson = new Gson();
        City city1 = gson.fromJson(json, City.class);

        System.out.println("citi1: " + city1.getId() + " " + city1.getName());*/

        Retrofit retrofit = new Retrofit.Builder().
                baseUrl("https://samples.openweathermap.org/data/2.5/").
                addConverterFactory(GsonConverterFactory.create()).
                build();

        WeatherService service = retrofit.create(WeatherService.class);

        Call<City> cityCall =  service.obtenerCiudadCelcius("Abra Pampa", "76b2cb12ebe6b81a654ce719218d5a1d", "metrics");

        cityCall.enqueue(new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                City city = response.body();
                System.out.println("city" + city.getName());
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                System.out.println("eroor");
            }
        });
    }

}
