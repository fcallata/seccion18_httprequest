package firstapp.test.fabio.seccion18_http_retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {

    @GET("weather")
    Call<City> obtenerCiudad(@Query("q") String city, @Query("appid") String key);

    @GET("weather")
    Call<City> obtenerCiudadCelcius(@Query("q") String city, @Query("appid") String key, @Query("units") String value);
}
